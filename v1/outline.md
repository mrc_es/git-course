# The (almost) the three GIT sections

## Git revisions

## 1. Workspace

## 2. Index/stage

## 3. Local repository

## a. Upstream repository

## b. Stash

* stash
* apply
* pop
* push

# Git log

Scope: Local and upstream repository

## Filters:

* since
* until 
* grep
* author
* string -G, -S
* By File --

# Diff

# Checkout

Scope: Local repository, index, workspace

## switch
## restore

# git rebase

Scope: workspace, local repository

## Commands:

* p, pick <commit> = use commit
* r, reword <commit> = use commit, but edit the commit message
* e, edit <commit> = use commit, but stop for amending
* s, squash <commit> = use commit, but meld into previous commit
* f, fixup <commit> = like "squash", but discard this commit's log message
* x, exec <command> = run command (the rest of the line) using shell
* b, break = stop here (continue rebase later with 'git rebase --continue')
* d, drop <commit> = remove commit
* l, label <label> = label current HEAD with a name
* t, reset <label> = reset HEAD to a label
* m, merge [-C <commit> | -c <commit>] <label> [# <oneline>]
# .       create a merge commit using the original merge commit's
# .       message( or the oneline, if no original merge commit was
# .       specified). Use -c <commit> to reword the commit message.

## Onto

# Cherry-pick

# Tags

# Bisect

# Resolve merge conflicts

# Fetch

Scope: local repository

# Push

# Pull

Scope: remote repository, local repository, workspace

* Multiple origins
* Expansions

# Config

## Info
## Aliases

# Reflog
