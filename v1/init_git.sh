#!/usr/bin/env bash

: << __docstring__
    Script created for a git training course

    Author: Marco Cuauhtli Elizalde Sanchez
__docstring__


name_to_mail_converter() {
    local -r COMPANY_EMAIL_DOMAIN="mape.moc"
    local -r name="$1"
    local -r name_snakecaseied="$(
        printf "$name" | tr '[[:upper:]][[:space:]]' '[[:lower:]]_')"
    echo "${name_snakecaseied}@${COMPANY_EMAIL_DOMAIN}"
}

#[START] (Commiter|author)s info 
readonly DAVID_AUTHOR_NAME="David Loeraienko"
readonly CARLOS_AUTHOR_NAME="Carlos Gomezov"
readonly HERNANDEZ_AUTHOR_NAME="Alemario Hernankov"
readonly SORIANO_AUTHOR_NAME="Carlos Sorianchuk"
readonly VICTOR_AUTHOR_NAME="Victor Carriliev"

readonly DAVID_MAIL="$(name_to_mail_converter "$DAVID_AUTHOR_NAME")"
readonly CARLOS_MAIL="$(name_to_mail_converter "$CARLOS_AUTHOR_NAME")"
readonly HERNANDEZ_MAIL="$(name_to_mail_converter "$HERNANDEZ_AUTHOR_NAME")"
readonly SORIANO_MAIL="$(name_to_mail_converter "$SORIANO_AUTHOR_NAME")"
readonly VICTOR_MAIL="$(name_to_mail_converter "$VICTOR_AUTHOR_NAME")"

export GIT_COMMITTER_NAME
export GIT_COMMITTER_DATE
export GIT_COMMITTER_EMAIL

export GIT_AUTHOR_NAME
export GIT_AUTHOR_DATE
export GIT_AUTHOR_EMAIL
#[END] (Commiter|author)s info 


#[START] Mock dates
readonly FOUR_WEEKS_STRING_DATE="4 weeks ago"
GIT_AUTHOR_DATE="$(date -d "$FOUR_WEEKS_STRING_DATE")"
GIT_COMMITTER_DATE="$GIT_AUTHOR_DATE"
#[END] Mock dates

#[START] Init files without commiting yet
touch master_{1..9} {david,carlos}_feature_{1..2} carlos_feature_{3..4}
#[END] Init files without commiting yet

init_master() {
    # Code and commits made by $HERNANDEZ_AUTHOR_NAME
    GIT_COMMITTER_NAME="$HERNANDEZ_AUTHOR_NAME"
    GIT_COMMITTER_EMAIL="$HERNANDEZ_MAIL"
    
    GIT_AUTHOR_NAME="$HERNANDEZ_AUTHOR_NAME"
    GIT_AUTHOR_EMAIL="$HERNANDEZ_MAIL"
    seq 1 4 | xargs -I % bash -c "git add master_%; git commit -am 'older master_%'"

    # Code and commits made by $VICTOR_AUTHOR_NAME
    GIT_COMMITTER_NAME="$VICTOR_AUTHOR_NAME"
    GIT_COMMITTER_EMAIL="$VICTOR_MAIL"
    
    GIT_AUTHOR_NAME="$VICTOR_AUTHOR_NAME"
    GIT_AUTHOR_EMAIL="$VICTOR_MAIL"
    seq 5 6 | xargs -I % bash -c "git add master_%; git commit -am 'older master_%'"

    # Putting actual dates for log-querying by date filter
    GIT_AUTHOR_DATE="$(date -d 'now')"
    GIT_COMMITTER_DATE="$GIT_AUTHOR_DATE"
    
    GIT_COMMITTER_NAME="$SORIANO_AUTHOR_NAME"
    GIT_COMMITTER_EMAIL="$SORIANO_MAIL"
    
    GIT_AUTHOR_NAME="$SORIANO_AUTHOR_NAME"
    GIT_AUTHOR_EMAIL="$SORIANO_MAIL"

    # New file for log-querying by commit name and content
    cat << _eof >> master_7
def funcion_nueva1():
    print("Funcion nueva1")
_eof

    #  Let's create a commit naming difference where "older" -> "four weeks ago",
    #+ and "newer" -> "now"
    git add master_7; git commit -am 'newer master_7: introduction of funcion_nueva1'

    # New file for log-querying by commit name and content
    cat << _eof >> master_8
def funcion_nueva2():
    print("Funcion nueva 2")
_eof

    git add master_8; git commit -am 'newer master_8: introduction of funcion_nueva2'6
    git add master_9; git commit -am 'newer master_9'
    
    # Let's reinitialize to the initial 4 weeks ago
    GIT_AUTHOR_DATE="$(date -d "$FOUR_WEEKS_STRING_DATE")"
    GIT_COMMITTER_DATE="$GIT_AUTHOR_DATE"
}

init_feature_branches() {
    # From the 2nd master commit we create the carlos branch
    git switch -d :/"older master_2"
    git switch -c carlos
    GIT_COMMITTER_NAME="$CARLOS_AUTHOR_NAME"
    GIT_AUTHOR_NAME="$CARLOS_AUTHOR_NAME"
    
    GIT_COMMITTER_EMAIL="$CARLOS_MAIL"
    GIT_AUTHOR_EMAIL="$CARLOS_MAIL"
    seq 1 4 | xargs -I % bash -c "
        git add carlos_feature_%; git commit -m 'carlos_feature_%'"

    # From the 2nd $CARLOS_AUTHOR_NAME commit, we create the david branch
    git switch -d :/carlos_feature_2
    git switch -c david
    GIT_COMMITTER_NAME="$DAVID_AUTHOR_NAME"
    GIT_COMMITTER_EMAIL="$DAVID_MAIL"

    GIT_AUTHOR_NAME="$DAVID_AUTHOR_NAME"
    GIT_AUTHOR_EMAIL="$DAVID_MAIL"
    seq 1 2 | xargs -I % bash -c "
        git add david_feature_%; git commit -m 'david_feature_%'"

    git switch master
}

init_master
init_feature_branches
