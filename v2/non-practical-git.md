# 1. Initialize a git repo

**Scenario**: Having the following file structure:

```plain
/my/awesome/codebase
|__ function1.py
|__ function2.py
|__ secrets.txt
|__ log.1.txt
```

We want to:

1) Initialize our repo setting a specific initial branch
2) Commit (protect) only files "function1.py", and "function2.py"
3) Verifying if only the files we want will be committed
4) Push the changes to the remote

**Approach**

For 1), let's run:

```bash
cd /my/awesome/codebase 
git init --initial-main=main
```

Here, we've created our **local** and we specified the *main* branch as the **initial branch**. 

Let's remember that back in time the standard initial branch was called *master*, but recently this is being changed in favor of *main*.

For 2), let's run the following:

```bash
git add function1.py function2.py
# Or for being fancy
git add function*.py
# Or more specific (and also fancy)
git add function{1,2}.py
```

With the `add` Git command what we're doing is to add specific files from our **working tree** to an specific git area called **index** or also "staging area".

This area is an important git structure which holds a snapshot which serves as the “staging area” between the files we have on our filesystem and our commit history.

In order to **commit** and save our changes in our local repo (aka, **local**),  we need to put our credentials, otherwise we'll have an error:

```bash
git config --global user.name "Cuauhtli Elizalde"
git config --global user.email "Cuauhtli_Elizalde@epam.com"
```

For 3), let's check the state of our repo:

```bash
$ git status
On branch main

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   function1.py
        new file:   function2.py

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        log.1.txt
        secrets.txt
```

And we've seen files "function1.py" and "function2.py" are ready to be committed (protected), meanwhile log and secret files are not.

Now we're ready for commit:

```bash
$ git commit -m "init: functions added"
[main (root-commit) e158bf5] init: functions added
 2 files changed, 4 insertions(+)
 create mode 100644 function1.py
 create mode 100644 function2.py
```

For 4), in order to push our changes to a remote repo (aka, **remote**), first let's indicate the name and url:

```bash
git remote add <name> <url>
```

For purpose of this exercise, we'll use:

```bash
git remote add origin file:///home/cuau-epam/win_documents/epam/git_course/remote-test
```

But we can use whatever name and an url which uses the four git transfer protocols accepted:

```bash
git remote add <name> [file://]<local path>
git remote add <name> ssh://[user:]host[:port]/...
git remote add <name> git@...
git remote add <name> https://...
```

Now let's save our local changes to our remote:

```bash
$ git push origin main
Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 8 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (4/4), 344 bytes | 11.00 KiB/s, done.
Total 4 (delta 0), reused 0 (delta 0), pack-reused 0
To file:///home/cuau-epam/win_documents/epam/git_course/remote-test
 * [new branch]      main -> main
```

## Conclusion

We've traveled to the majority of the git areas in order: **working tree**, **index**, **local**, **remote**.

**Git concepts**: local, remote, index (staging area), initial branch, commit message.

**Git commands**: *init, add, commit, status, push, remote, config*.

# 2. Put our changes on top of another branch

**Scenario**: we've created a feature with our commits, and now we want to integrate to a *main* branch

<img src="./img/image-20230217004949692.png" alt="image-20230217004949692" width="50%" />

**Approach 1**: We can use *git rebase* for this:

```bash
git rebase main our-feature #(1) git rebase <base branch> <branch we want at the top>
git switch main             #(2) Next, let's move to main because the rebase moved us
git merge our-feature       #(3) Finally, lets merge (fast forward) our feature into main
```

Let's do it step by step:

1. Having this by running `git log --all --decorate --oneline --pretty`:

   <img src="./img/image-20230217013021787.png" alt="image-20230217013021787" width="40%" />

Let's rebase `our-feature` branch

```bash
$ git rebase main our-feature
Successfully rebased and updated refs/heads/our-feature.
```

Let's take a look at our commit tree:

<img src="./img/image-20230217013516295.png" alt="image-20230217013516295" width="50%" />

Now, we've rebased our branch, but we need to apply those changes to *main*, so let's go to 2nd and 3rd steps:

```bash
$ git switch main
Switched to branch 'main'
$ git merge our-feature
Updating ab4cc7f..903b395
Fast-forward
 our_feature_file_1 | 1 +
 our_feature_file_2 | 1 +
 our_feature_file_3 | 1 +
 3 files changed, 3 insertions(+)
 create mode 100644 our_feature_file_1
 create mode 100644 our_feature_file_2
 create mode 100644 our_feature_file_3
```

Let's see again at our graph:

<img src="./img/image-20230217013736672.png" alt="image-20230217013736672" width="50%" />

And now our two branches are on the same commit and we've created a linear graph.

**Approach 2**: We can use also *cherry-pick*:

```bash
$ git switch main  # (1)
$ git cherry-pick <parent of the bottom of our-feature branch>..our-feature  # (2)
$ git branch -D our-feature  # (3)
```

First, let's look at our graph:

<img src="./img/image-20230217014814926.png" alt="image-20230217014814926" width="40%" />

Now let's use 1 and 2:

```bash
$ git switch main
$ git cherry-pick 03a2321^..our-feature
```

Let's take another look at our graph:

<img src="./img/image-20230217015209641.png" alt="image-20230217015209641" width="40%" />

Now we can remove *our-feature* branch with `git branch -D our-feature`, and we got:

<img src="./img/image-20230217015320334.png" alt="image-20230217015320334" width="40%" />

And we've got what we wanted.

**Git concepts**: cherry pick, graph commit tree, revisions, ranges, merge, rebase, fast forward, removing branches, HEAD.

**Git commands**: *rebase, merge, log, cherry-pick, branch, switch*

# 3. Non-linearizing branches

**Scenario 1**: For some reason we want to pass from one commit diagram to the other. In other words, when we type `git log --all --decorate --oneline --graph`, we wanted to see exactly this diagram with this orientation:

<img src="./img/non-linear1.PNG" alt="non-linear1" width="50%" />

**Approach 1**:

```bash
git tag new-head HEAD~3                # (1) Create a tag called "new-head" pointing the 3rd commit 
                                       #+ backwards HEAD
git tag branch-base HEAD~5             # (2) Create a tag called "branch-base" pointing the 5th
                                       #+ commit backwards HEAD
git switch -c mybranch branch-base     # (3) Create/locate a new branch with base in "branch-base"                                          #+ tag
git cherry-pick new-head..main         # (4) Apply the commits in the left-open range 
                                       #+ (new-head, main]
git switch main                        # (5) Let's go back to main
git reset --hard branch-base           # (6) We reset main to "branch-base" tag
git cherry-pick branch-base..new-head  # (7) Apply commits in the range (branch-base, new-head]
git tag -d new-head branch-base        # (8) Remove the tags "new-head" and "branch-base"
```

And the result of copypasting will be:

<img src="./img/non-linear-final.PNG" alt="non-linear-final" width="40%" />

Let's colorize the git objects for making it more clear.

<img src="./img/e5-result-diagram.PNG" alt="e5-result-diagram" width="50%" />

**Approach 2**: We can also use a `git rebase --onto` approach:

```bash
git rebase --onto HEAD~5 HEAD~3 HEAD  #  For applying to "HEAD~5" commit, from "HEAD~3"
                                      #+ to the current HEAD
git switch -c "mybranch"  #  From the past command, we've switched to the new applied commits,
                          #+ so now we'll make an oficial branch with these
git switch main  # We move to main after we've created the branch we want
git reset --hard HEAD~3  #  Now we've protected the last commits in a branch, we can change
                         #+ the HEAD to the new location
```

Now, if we take a look at the graph with `git commit --all --decorated --oneline --graph`:

<img src="./img/e3_almost_finish.PNG" alt="image-20230309014635477" width="45%" />

And we almost have it!... with the slightly difference that we have changed the direction of our graph. We wanted the main in the left-most side.

So, let's just change the last commit message using `git commit --amend -m "add: file 6 new message"`

<img src="./img/e3_finish.PNG" alt="image-20230309014937068" width="45%" />

And finally we got the orientation we wanted.

**Git concepts**: tags, revisions, cherry pick, commit graph, rebase "--onto", amend.

**Git commands**: *tag, switch, cherry-pick, reset, log, commit, rebase*.

# 4. Cleaning unused branches

**Scenario**: Imagine we have a lot of branches we've already merge or we don't use (maybe cuz are duplicated), and this just make our graph confusing:

<img src="./img/e4_initial.PNG" width="60%" />

So we can identify two class of branches:

1. Merged that should be deleted
2. Unmerged that will never be used

**Approach**:

For point 1, let's identify the merged branches and their creators with a **Bash** command line:

```bash
{
    echo "REF name|Commit date|Buddy|Subject" 
    {
        git branch -r --merged origin/main \
        	| grep -Ev 'origin\/(main|master)$' \
	        | xargs git show -s --format="%D:%h|%cs|%ce|%s" \
    	    | sort -rk2 --field-separator="|"
    }
} | column -ts "|"
```

Resulting in something like:

```plain
REF name                                         Commit date  Buddy                       Subject
origin/branch_merged_2, branch_merged_2:87f8641  2023-03-08   carlos_sorianchuk@mape.moc  commit merged 5
origin/branch_merged_1, branch_merged_1:f24fa0b  2023-02-08   carlos_sorianchuk@mape.moc  commit merged 2
```

Let's take a closer look at our command-line:

* `git branch -r --merged origin/main`, for listing the remote branches merged to *origin/main*

* `grep -Ev "origin\/(main|master)$"`, for excluding from our report the *main* or *master* remote branches

* `xargs git show -s --format="%D:%h|%cs|%ce|%s"`, for getting, for each branch listed, a record with format:

   `<simple refname>:<short commit hash>|<committer date>|<committer email>|<subject>`

* `sort -rk2 --field-separator="|"`,  for sort records from newest to oldest using "|" as field separator

* `column -ts "|"`, for pretty print the results in tabular format using "|" as the field separator

With this info **we can contact with the creator of the commits and ask them if we can remove the branches**.

**Only when the answer of everybody is affirmative**, we can proceed to delete them manually using the GUI, or automatically with another fancy one-liner:

```bash
git branch -r --merged origin/main \
    | grep -Ev 'origin\/(main|master)$' \
    | grep -Po 'origin/\K.*' \
    | xargs -I % bash -c "git push origin --delete % && git branch -D %"
git fetch -p
```

This command is similar from previous, but let's address some points:

* `grep -Ev 'origin\/(main|master)$'`, **is pretty important in order to not mess with our stable branch**
* `grep -Po 'origin/\K.*'`, for capturing only the branch names after the `origin/` string
* `xargs -I % bash -c "git push origin --delete % && git branch -D %"`, for each branch name, we'll delete it from the remote and the local
* `git fetch -p`, for pruning or removing remote-tracking's references no longer existing on the remote

For point 2, i.e., removing non merged branch, let's make something similar:

```bash
{
    echo "REF name|Commit date|Buddy|Subject" 
    { 
        git branch -r --no-merged origin/main \
            | grep "origin/" \
            | xargs git show -s --format="%D:%h|%cs|%ce|%s" \
            | sort -rk2 --field-separator="|"
    }
} | column -ts "|"
```

Note that the only significant difference is the `--no-merged` parameter and that we don't filter the stable branch.

```plain
REF name                                           Commit date  Buddy                       Subject
origin/stalled_branch_1, stalled_branch_1:3d98e0e  2023-03-13   david_loeraienko@mape.moc   stalled commit 2
origin/stalled_branch_2, stalled_branch_2:19eee49  2023-03-13   carlos_sorianchuk@mape.moc  stalled commit 5
```

Now we have the unmerged branches, we can dig in the purpose of these branches by using commands like `git show` or `git log` and see the changes tried to introduce.

**We must ask each author if we can delete their branches**. Only **if we have an affirmative answer**, we can proceed delete it manually or automatically with another fancy bash script pretty similar to the previous for merged branches:

```bash
git branch -r --no-merged origin/main \
	| grep -Po "origin/\K.*" \
	| xargs -I % bash -c "git push origin --delete % && git branch -D %"
git fetch -p
```

If is the case we got a negative answer for deleting some branch, we can perform successive runs with the branches expected to delete:

```bash
git push origin --delete <branch to delete> && git branch -D <branch to delete>
```

And finally, `git fetch -p`.

Finally, after run these one-liners, we will see a cleaner commit graph if we run `git log --all --decorate --oneline --pretty`:

<img src="./img/e4_final_result.PNG" width="40%" />

**Git concepts**: commit author and date, remote, local, commit graph, merged and unmerged branches, pruning, formatting commit tree info, remove local and remote branches.

**Git commands**: *show, fetch, branch, push*.

# 5. Push and fetch changes from other repositories

**Scenario 1**: For some reason, you need to update another git repo, different from your origin, with a pushed changes to the origin.

**Approach 1:** Let's simply do the following steps:

```bash
git remote add new_origin <new origin url>  # In case we haven't updated the new origin
git push --all new_origin
```

If we perform theses steps and then a `git log --all --decorate --oneline --graph`, we'll see something like:

<img src="./img/e5_final_result.PNG" alt="image-20230309125325928" width="50%" />

And now we'll see both remotes (*origin* and *new_origin*) pointing to the same commit.

But what if more commits are being push into the almost old origin? For example, now we have this:

<img src="./img/e5_more_changes.PNG" alt="image-20230309130147466" width="50%" />

And now the *origin/dev* remote-tracking is ahead of *new_origin/dev*.

We can just pull the *origin* changes, and push them again into *new_origin*:

```bash
git pull origin dev
git push new_origin dev
```

**Approach 2:** We can ask the origin committers to append a new push URL in their origin remote repo:

```bash
git remote set-url --add --push origin <first origin url>
git remote set-url --add --push origin <second origin url>
```

Now, when they push the changes with something like `git push origin dev`, they'll push to both remotes. Both under the same name "origin":

```bash
$ git push origin dev
Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Delta compression using up to 8 threads
Compressing objects: 100% (6/6), done.
Writing objects: 100% (6/6), 641 bytes | 49.00 KiB/s, done.
Total 6 (delta 3), reused 0 (delta 0), pack-reused 0
To file:///home/cuau-epam/win_documents/epam/git_course/remote_repos/second_origin/
   06972b5..6cf80dc  dev -> dev
Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Delta compression using up to 8 threads
Compressing objects: 100% (6/6), done.
Writing objects: 100% (6/6), 641 bytes | 49.00 KiB/s, done.
Total 6 (delta 3), reused 0 (delta 0), pack-reused 0
To file:///home/cuau-epam/win_documents/epam/git_course/remote_repos/first_origin/
   06972b5..6cf80dc  dev -> dev
```

For looking at those changes, with what we've already done, now we must fetch the *new_origin* branch: `git fetch new_origin`, and we'll see the changes under two names, "origin" and "new_origin":

<img src="./img/e5_final_result_2.PNG" alt="image-20230309151246761" width="60%" />

**Approach 3:** We can use *git hooks*, which, in shot terms, are executable scripts that are triggered once a specific git action is performed.

For this approach, **we'll assume we have the complete control of the remote**, which in common projects is not the case, since we store our remote in a managed repository systems that have full control of our repos like GitLab or GitHub.

First, let's create a server git hook Bash script in the file `/<our remote repo path>/hooks/post-update` with the content:

```bash
#!/usr/bin/env bash
new_origin_url="file:///home/cuau-epam/win_documents/epam/git_course/remote_repos/second_origin/"

git remote -v | grep "new_origin"
is_new_origin_defined="${PIPESTATUS[1]}"

# If is not defined, we'll define it
[ "$is_new_origin_defined" -eq 1 ] && git remote add new_origin "$new_origin_url"

echo >&2 "[INFO] Pushing all branches to $new_origin_url"
git push --all new_origin
```

Next, let's assign execution permissions: `chmod +x post-update`

With this git hook, after the remote have updated its data successfully, we'll instruct to create the *new_origin* remote pointing to the new remote URL, and then push all the branches of the current to the new remote.

The next time we'll push, we'll see something like this in the logs:

```bash
$ git push origin dev
Enumerating objects: 9, done.
Counting objects: 100% (9/9), done.
Delta compression using up to 8 threads
Compressing objects: 100% (8/8), done.
Writing objects: 100% (8/8), 860 bytes | 57.00 KiB/s, done.
Total 8 (delta 4), reused 0 (delta 0), pack-reused 0
remote: new_origin      file:///home/cuau-epam/win_documents/epam/git_course/remote_repos/second_origin/ (fetch)
remote: new_origin      file:///home/cuau-epam/win_documents/epam/git_course/remote_repos/second_origin/ (push)
remote: [INFO] Pushing all branches to file:///home/cuau-epam/win_documents/epam/git_course/remote_repos/second_origin/
remote: To file:///home/cuau-epam/win_documents/epam/git_course/remote_repos/second_origin/
remote:    06972b5..6c4300e  dev -> dev
To file:///home/cuau-epam/win_documents/epam/git_course/remote_repos/first_origin/
   06972b5..6c4300e  dev -> dev
```

And, after a `git fetch --all` to the remotes, we'll see local and remotes updated at the same HEAD level:

<img src="./img/e5_final_result_3.PNG" alt="image-20230309170843009" width="60%"/>

**Git concepts**: remotes, origin, git hooks, post-update git hook, multiple remote urls, push and fetch urls.

**Git commands**: *push, fetch, pull, remote*.

# 6. Git rebase and git merge integrating changes

One thing we found in a lot of scenarios is the question about to merge or to rebase. Both commands are not the same but can perform one thing in similar: to integrate multiple branches into another.

Let's imagine a cool scenario where a team is in charge of create a branch with the biographies of the metal bands leaded by a woman, but there are a lot of music genres and that could lead to a confusion when jumping from one to another genre.

The team decided to divide in multiple groups each one in charge of one genre. At the end, they'll integrate the biographies founded in one big branch.

To uniform the changes, they'll follow a commit naming convention like: `<genre lowercased>:<band name title cased>`:

<img src="./img/e6_initial.PNG" alt="image-20230310003003256" width="45%;" />

**Approach 1**:

Now, If we first switch to *main*, and then perform a:

```bash
git merge gothic symphonic_metal metalcore nu_metal mathcore sludge
```

We'll get something like:

<img src="./img/e6_merge.PNG" alt="merge" width="50%;" />

For avoiding the branch mess that *merge* does, we can use the `--squash` parameter:

```bash
 git merge --squash gothic symphonic_metal metalcore nu_metal mathcore sludge
```

Then, we'll be prompted to create a merge commit message. After that, we'll need to commit those changes:

```bash
git commit
```

And we'll get something like:

<img src="./img/e6_merge_squash.PNG" alt="image-20230310021306448" width="40%;" />

But let's notice we have an extra commit at the top of the history. **This commit contains all the applied changes from all the branches involved in the merge**. And now we can remove the branches with `git branch -D`, then we'll see a shorter version, but not so descriptive like the last:

<img src="./img/e6_merge_squash_clean.PNG" alt="merge" width="40%;" />

**That is the importance of creating a very descriptive merge commit message subject and body**. If we avoid that, we'll lose information about the squashed commits.

**Approach 2:**

By the other hand, if we want to use *rebase*, we'll need to perform multiple times the `git rebase` command. For example, first, let's locate in *main*, and then let's type:

```bash
git rebase sludge
git rebase mathcore
git rebase nu_metal
git rebase metalcore
git rebase symphonic_metal
git rebase gothic
```

And if we had no conflicts, let's remove the rebased branches:

```bash
git branch -D gothic symphonic_metal metalcore nu_metal mathcore sludge
```

And we'll see something like this:

<img src="./img/e6_rebase.PNG" alt="rebase" width="40%;" />

And now we see a linear history for our teamwork. Also we can guide us through the commit names.

We also can perform this kind of squashing with *rebase* by using:

```bash
git rebase sludge
git rebase mathcore
git rebase nu_metal
git rebase metalcore
git rebase symphonic_metal
git rebase gothic
git branch -D gothic symphonic_metal metalcore nu_metal mathcore sludge
```

And after having our linear history, we can squash with:

```bash
git rebase -i ":/init: added README"
```

Next, we'll get prompted the *git-rebase-todo*.

Now, we must put the commits that begins with "init" together, and change the "pick" word for the "s" letter in order to squash, except just for one commit tat we'll use for create the squash commit:

```
pick b6caad6 init: gothic
pick 52fa71b init: symphonic_metal
pick 951ad24 init: metalcore
pick dfb20db init: nu_metal
pick 5c567c4 init: mathcore
pick 56ee1f7 init: sludge
pick 2f49967 gothic: Lacuna Coil
s 0ee8bd2 gothic: Evanescence
s 9a0f5ed gothic: Unsun
s ...
...
```

And continuing putting "s" at the beginning of each listed commit. Then save and close the editor.

Now, we'll see a prompt asking us for creating/choosing a name for the commit that will have the content of all the squashed commits. Let's just write "Integrating changes", and then close the editor. Now, we'll have something pretty similar to our merge scenario:

<img src="./img/e6_rebase_squash.PNG" alt="rebase squash" width="40%;" />

## Conclusion

We saw the differences between *merge* and *rebase*.

With *rebase* we have **more control of the branching integration**, meanwhile with *merge* we have the basics and pretty straightforward merging mechanisms. 

Lastly, we saw superficially the importance of writing a good commit message and body, because depending our integration strategy, we can lose some information like the content of a commit, or information about the creation.

**Git concepts**: merge, squash, rebase, commit message, interactive rebase, pick.

**Git commands**: *merge, rebase, branch*.

# 7. Integrating commits that passed the tests

**Scenario**: We have a repository with a branch called *filters* with multiple commits not tested, and we only want to integrate those that will pass the tests. The tests were created but not executed for whatever reason:

<img src="./img/e7_init.PNG" alt="image-20230310185105032" width="50%;" />

**Approach:** Let's use rebase for accomplish this, with the *exec* command in the *git-rebase-todo*.

First, let's switch to *filters* and next, let's rebase *main* interactively:

```bash
git switch filters
git rebase -i main
```

Now, let's edit the *todo*:

<img src="./img/e7_rebase-todo.PNG" alt="image-20230310185855977" width="50%;" />

And when we close it, we'll found the result of the command executed:

<img src="./img/e7_rebase-todo-executed.PNG" alt="image-20230310185855977" width="90%;" />

And we see the test failing when testing the function `is_less_than_10_chars`

After we fix the function, we must mark that issue as a resolved by doing *add* and *commit*, and next continue the rebase operation:

```bash
git add <fixed file>
git commit 
git rebase --continue
```

And now our branch will be rebased:

<img src="./img/e7_final.PNG" alt="image-20230310191725399" width="50%;" />

**Git concepts**: *exec* and *pick* rebase command, *rebase-todo* file, interactive rebase.

**Git commands**: *add, commit, rebase*.

# 8. Restoring files from index and from commit tree

**Scenario:** We look that, after various commits, we need a settings file we've deleted in a past commit. Also we accidentally deleted some files we've added to the index some minutes ago.

We want the settings, and the files we accidentally deleted.

**Approach**:

First let's see the git tree with `git log --all --decorate --oneline --pretty`:

<img src="./img/e8_log.png" alt="image-20230313161522941" width="50%;" />

We can see a commit message, "chore: settings removed", so that is a good place for looking. First let's try with a `git show ":/chore: settings removed"`:

<img src="./img/e8_show.png" alt="image-20230313161829875" width="40%;" />

And we se that the *settings.py* file was deleted. We can copy the values and create a new settings file, but let's use *git* with the subject before the file was deleted: 

```bash 
git restore --source ":/chore: added settings" settings.py
```

Now, if we list our working directory:

```bash
$ ls
filters/  main.py  processors/  settings.py
```

We'll see the `settings.py` file again.

Now, for restoring the files we added to the index (or *staging area*), let's ask for the status:

```bash
$ git status
```

```bash
On branch main
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   filters/avro_filters
        new file:   filters/orc_filters
        new file:   filters/parquet_filters

Changes not staged for commit:
  (use "git add/rm <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        deleted:    filters/avro_filter
        deleted:    filters/orc_filter
        deleted:    filters/parquet_filter

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        settings.py
```

Now, as the status message says, let's type:

```bash
git restore filters/avro_filters
git restore filters/orc_filters
git restore filters/parquet_filters
```

And now we'll get:

```bash
$ git status
On branch main
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   filters/avro_filters
        new file:   filters/orc_filters
        new file:   filters/parquet_filters

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        settings.py
```

Finally, let's add to the index the *settings.py* file:

```bash
$ git add settings.py
$ git commit -m "fix: restored filters and settings file"
[main b3fcd55] fix: restored filters and settings file
 4 files changed, 8 insertions(+)
 create mode 100644 filters/avro_filters
 create mode 100644 filters/orc_filters
 create mode 100644 filters/parquet_filters
 create mode 100644 settings.py
```

And we've committed the filters we've recently created and also restored the *settings.py*.

**Git concepts**: index, commit tree.

**Git commands**: *log, show, status, restore, add, commit*.

# 9. Apply commits to multiple branches

**Scenario:** There is a specific commit that will allow the branches you're working to get authentication and connectivity. Instead manually copy and paste your changes per branch, you want to copy your changes to all branches.

The example tree is the following:

<img src="./img/e10_init.PNG" alt="image-20230313180841937" width="45%;" />

**Approach**: First, we need to ask the team that we'll be applying the changes to all branches, next we can perform with:

```bash
git switch branch_1; git cherry-pick 1bc5d7a ac1f608
git switch branch_2; git cherry-pick 1bc5d7a ac1f608
git switch branch_3; git cherry-pick 1bc5d7a ac1f608
```

And finally we'll get:

<img src="./img/e10_final.PNG" alt="image-20230313181425319" width="45%;" />

And we've applied all urgent commits to all our branches.

Notice the applied commit has different hash at the top of each the branch, but the same commit subject.

**Git concepts**: cherry pick.

**Git commands**: *switch, cherry-pick*.

# 10. Playing with the stash

**Scenario**: we are in a working directory with uncommitted and unstaged work and we want to apply one stash entry with the label "function_6", commit, and then keep working in the uncommitted changes.

**Approach**: First, let's stash our changes with:

```bash
git stash push -m "WIP: my changes" -u
```

Here, we've create a new entry at the top of the existing ones with a message "WIP: my changes".

The parameter *-u*, is used for include the *untracked* files, i.e. the files that are not added to the index.

If we look at our stash index with `git stash list`:

```
stash@{0}: On main: WIP: my changes
stash@{1}: On main: WIP: function 7
stash@{2}: On main: WIP: function 6
stash@{3}: On main: WIP: function 5
stash@{4}: On main: WIP: function 4
```

Now, for see the content of the `stash@{2}` entry, let's type `git stash show 2 -u -t`:

```
diff --git a/function_6.py b/function_6.py
new file mode 100644
index 0000000..5d3d498
--- /dev/null
+++ b/function_6.py
@@ -0,0 +1,2 @@
+def function_6():
+    print("this is function 6")
```

Now, for only apply the *function 6* stash, we can type `git stash apply 2`, and our status log should throw something like this:

```bash
Already up to date.
On branch main
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        function_6.py

nothing added to commit but untracked files present (use "git add" to track)
```

Let's commit with:

```bash
git add function_6.py
git commit -m "feature: added function_6"
```

And let's reapply our last changes using `git stash pop`:

```bash
$ git stash pop 0
Already up to date.
On branch main
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        changes

nothing added to commit but untracked files present (use "git add" to track)
Dropped refs/stash@{0} (7a6678bdf6ed258c31afe142a5712737c7a793f2) 
```

And we have again our changes and dropped the entry from the stash stack.

**Git concepts**: stash, apply, pop, push, list, stash stack.

**Git commands**: *stash, add, commit*.

# 11. Looking for info in the commit history

**Scenario**: We need to find:

1)  A commit:
   - Made by an author called "Carlos"
   - That was committed on December 2020
   - Because we're searching the commit that introduces a function called something like, "filter_is_numeric" and "hyphen"
2) Also we need to know who and when introduced the function *filter_is_alpha*

When running `git log --all --decorate --oneline --pretty` we have something like:

<img src="./img/e11_init.PNG" alt="image-20230315142326656" width="35%;" />

**Approach**: 

For 1), let's use this for look the commits created by the Carlos's authors and within the dates *2020-12-01* and *2020-12-20*:

```bash
git log --author="Carlos" --since=2020-12-01 --until=2020-12-30
```

<img src="./img/e11_log_author.PNG" alt="image-20230315142954630" width="45%;" />

Now, let's try to reduce the result by searching for the given pattern:

```bash
git log \
    --author="Carlos" \
    --since=2020-12-01 \
    --until=2020-12-30 \
    -G "filter_is_numeric.*hyphen" \
    --format=fuller
```

And we get:

<img src="./img/e11_grep_result.PNG" alt="image-20230315143231984" width="45%;" />

We found the commit with the exact creation date, author name and mail, and commit name. Now let's see the content:

```bash
$ git show ":/carlos_feature_5"
$ git show ":/carlos_feature_5" --  #  If we have a file named like the revision we're looking,
                                    #+ "--" removes the ambiguity
```

And there is what we were searching:

<img src="./img/e11_show_result1.PNG" alt="image-20230315144213213" width="45%;" />

For 2), since we have only the name of the function, let's look with:

```bash
$ git log -S "def filter_is_alpha(" --format=fuller
```

And we get:

<img src="./img/e11_string_result.PNG" alt="image-20230315144601142" width="45%;" />

Now, let's see the content of the revision:

```bash
$ git show --format=fuller ":/david_feature_4" --
```

<img src="./img/e11_show_result2.PNG" alt="image-20230315144213213" width="45%;" />

And we have the introduction of `filter_is_alpha` function.

**Git concepts**: *git log* filter by date range, string, regular expression, author, pretty print, drop ambiguity in *git show* command, author date, commit date.

**Git commands**: *log, show*.

# 12. Reverting changes

**Scenario:** Sometimes we found our changes are not needed or we found an issue. But sometimes the changes we've introduced are a lot and in multiple files so we don't remember exactly what we did.

Let's suppose we have this commit graph:

<img src="./img/e12_init.PNG" alt="image-20230315160242851" width="45%;" />

We want to switch to "our-feature" branch, and restore the branch until the "feature 3".

**Approach 1**:

A clear and verbose way ("verbose" in a good way) of doing this is using `git revert`:

```bash
$ git switch our-feature
$ git revert --no-edit 9ae348b..8a9a2b8  # From left open range (feature 3, feature 8]
```

And we'll get in our graph:

<img src="./img/e12_revert.PNG" alt="image-20230315161407052" width="50%;" />

We have new commits that revert the commits we've specified in the range.

We can revert commit per commit, but we choose this options due the simplicity. Just we must pay attention that the range "A commit..B commit", is a left open range, that will took all commits between A and B, but without the A commit.

The benefits of this approach are the clear message about what we are doing (we can drop the `--no-edit` option and place a specific message), and that we have the commits we've reverted, so we can see into those commits in the future.

**Approach 2:**

We can use the `git reset` command. This command will not revert the changes, just will place the branch head into another commit. 

When we type:

```bash
$ git reset ":/feature: add feature 3" 
```

We'll move the branch HEAD to the respective commit that introduces the "feature 3". This will discard the other commits:

<img src="./img/e12_reset.PNG" alt="image-20230315162818033" width="50%;" />

And now we see **there are no other commits at the top of the one that introduced the feature 3**.

These can be cleaner, but not descriptive and also it is destructive.

We can reach the other commits just by using their commit hash id... if we remember it.

For example, now we've reset the branch. If we remember, for example, the commit hash for the "feature 8", we can still use that reference, and we can still operate with another git commands:

```bash
$ git log 8a9a2b88
```

<img src="./img/e12_recover_1.PNG" alt="image-20230315163341213" width="50%;" />

Also, if we reset for a mistake, we can recover immediately with:

```bash
$ git reset ORIG_HEAD
```

**The condition is that the *ORIG_HEAD* revision, should not be changed by another *git* command.**

Another way for recover from a reset, is using *cherry-pick* with the rescued commits we've dropped. **But this implies we know the dropped commit**. If that is the case, we can re-apply them with:

```bash
$ git cherry-pick 9ae348b^..8a9a2b8
```

<img src="./img/e12_recover_cherry_pick.PNG" alt="image-20230315164210328" width="45%;" />

And now we'll have the dropped commits but reapplied and with new commit hashes.

If we don't know the commit hashes, we can still figure what happened using the `git reflog` command.

For example, having this new tree and resetting to the "feature 3" commit:

<img src="./img/e12_init2.PNG" alt="image-20230315160242851" width="45%;" />

The *reflog* command result is something like:

<img src="./img/e12_reflog.PNG" alt="image-20230315165310486" width="70%;" />

And in the left side we can see the commit changes the HEAD had.

And, in the entry with the string "HEAD@{4}", we have the commit that introduced the "feature 8" also with the hash "319b2a3".

By knowing this, we can or apply the commits we want with *cherry-pick*, or reset the HEAD's branch to an specific commit, and this will "restore" the changes.

For example, we can reset to the "feature 7", and we'll have the previous 4, 5, and 6:

```bash
$ git reset HEAD@{5} # This is an option refering the position in the reference logs (reflog)
$ git reset d40e163  # This is another option refering to the commit
```

<img src="./img/e12_recover_reflog.PNG" alt="image-20230315165845259" width="50%;" />

**Let's remember the reference log have an expiration date of 90 days**.

**Git concepts**: revert changes, reference log, commit range, cherry pick, ORIG_HEAD, recover from a reset.

**Git commands**: *reset, revert, cherry-pick, reflog*.
